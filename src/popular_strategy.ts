import { IStrategy } from './interface_strategy';
import { Movie } from './movie';
import { RequestHelper } from './requestHelper';

export class PopularStrategy implements IStrategy{
    getMovies(page : number): Promise<Array<Movie>> {
        return new RequestHelper().makeRequest('popular', page);
    }
}
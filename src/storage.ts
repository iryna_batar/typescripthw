export class Storage {
    favoriteMoviesId : Array<number> = [];

    constructor() {
        if(window.localStorage.getItem('favoriteMovies') !== null) {
            this.favoriteMoviesId = JSON.parse(window.localStorage.getItem('favoriteMovies'));
        }
    }

    removeArr(movieid: number) {
        window.localStorage.setItem('favoriteMovies', JSON.stringify(this.getFavoriteMovies().filter(e => e !== movieid)));
    }

    addLocalStorage(movieid: number) {
        this.getFavoriteMovies().push(movieid);
        window.localStorage.setItem('favoriteMovies', JSON.stringify(this.getFavoriteMovies()));
    }

    getFavoriteMovies() {
        return this.favoriteMoviesId;
    }

    has(movieid: number) {
        return this.getFavoriteMovies().filter(e => e.toString() === movieid.toString()).length > 0;
    }
}
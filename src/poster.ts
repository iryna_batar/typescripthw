import { IStrategy } from './interface_strategy';
import { Movie } from './movie';
import { TopRatedStrategy } from './top_rated_strategy';
import { PopularStrategy } from './popular_strategy';
import { UpcomingStrategy } from './upcoming_strategy';
import { Storage } from './storage';
import { RequestHelper } from './requestHelper';
import { SearchStrategy } from './search_strategy';

export class App {

    strategy : IStrategy;
    movies : Array<Movie> = [];
    page = 1;
    movieContainer = document.getElementById('film-container');
    favoriteListener = (e : any) => this.changeFavorite(e.currentTarget);

    constructor(strategy : IStrategy) {
        this.strategy = new PopularStrategy();

        document.getElementById('top_rated')?.addEventListener('click', () => {
            this.strategy = new TopRatedStrategy();
            this.changeStrategy();
        });

        document.getElementById('popular')?.addEventListener('click', () => {
            this.strategy = new PopularStrategy();
            this.changeStrategy();
        });

        document.getElementById('upcoming')?.addEventListener('click', () => {
            this.strategy = new UpcomingStrategy();
            this.changeStrategy();
        });

        document.getElementById('load-more')?.addEventListener('click', () => {
            this.loadMore();
        });

        document.getElementById('submit')?.addEventListener('click', () => {
            this.strategy = new SearchStrategy();
            this.changeStrategy();
        });
    }

    public getMovies() : void {
        this.strategy.getMovies(this.page)
            .then((data : Array<Movie>) => {
            this.movies = data;
            this.page += 1;
            this.printMovies();
            const randomFilm = Math.floor(Math.random() * this.movies.length);
            App.printRandomMovie(this.movies[randomFilm]);
        });
    }

    private static printRandomMovie(movie : Movie)  : void {
        const name = document.getElementById('random-movie-name');
        const description = document.getElementById('random-movie-description');
        const container = document.getElementById('random-movie');
        description.innerText = movie.overview;
        name.innerText = movie.title;
        container.style.background = `url(${movie.poster}) no-repeat`;
        container.style.backgroundSize = `cover`;

    }

    private setStrategy(strategy : IStrategy) {
        this.strategy = strategy;
    }

    public printMovies()  : void {
        for (const movie of this.movies) {
            this.movieContainer.innerHTML += movie.printCard();
        }

        this.addLikeListeners();
    }

    private addLikeListeners()  : void {
        const elements = document.getElementsByClassName('bi');

        for (let i = 0; i < elements.length; i++) {
            elements[i].removeEventListener('click', this.favoriteListener);
        }

        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener('click', this.favoriteListener);
        }
    }

    private changeStrategy()  : void {
        this.movieContainer.innerHTML = "";
        this.page = 1;
        this.getMovies();
    }

    private loadMore()  : void {
        this.getMovies();
    }

    private changeFavorite(element : HTMLElement )  : void {
        const movieid = element.dataset.movieid;
        const storage : Storage = new Storage();
        if(storage.has(movieid)) {
            storage.removeArr(movieid);
            element.setAttribute("fill", "#ff000078")
        } else {
            storage.addLocalStorage(movieid);
            element.setAttribute("fill", "red")
        }

        this.updateFavorite();
    }


    private updateFavorite()  : void {
        const blockOfFavorite = document.getElementById('favorite-movies');
        blockOfFavorite.innerHTML = '';
        const storage : Storage = new Storage();
        const favoriteId = storage.getFavoriteMovies();
        const requestHeloer = new RequestHelper();
        for(const id of favoriteId) {
            requestHeloer.makeRequestById(id).then(data => {
                blockOfFavorite.innerHTML += data.printFavoriteCard();
            });
        }
        this.addLikeListeners();
    }
}


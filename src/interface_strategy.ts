import { Movie } from './movie';

export interface IStrategy {
    getMovies(page : number) : Promise<Array<Movie>>;
}
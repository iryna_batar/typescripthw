import { createElement } from './createElement';
import { Storage } from './storage';

export class Movie {
    poster: string;
    id: number;
    overview: string;
    releaseDate: string;
    title: string;
    imagesUrl = 'https://image.tmdb.org/t/p/w500';
    storage : Storage;

    constructor(data: IMovie) {
        this.poster = this.imagesUrl + data.poster_path;
        this.id = data.id;
        this.overview = data.overview;
        this.releaseDate = data.release_date;
        this.title = data.title;
        this.storage = new Storage();
    }

    printCard() {
        return `<div class="col-lg-3 col-md-4 col-12 p-2">
            <div class="card shadow-sm">
                 <img src="${this.poster}"/>
                    <svg
                        data-movieid="${this.id}"
                        xmlns="http://www.w3.org/2000/svg"
                        stroke="red"
                        fill="${this.storage.has(this.id) ? "red" : "#ff000078" }"
                        width="50"
                        height="50"
                        class="bi bi-heart-fill position-absolute p-2"
                        viewBox="0 -2 18 22">
                        <path
                            fill-rule="evenodd"
                            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                        />
                    </svg>
                    <div class="card-body">
                        <p class="card-text truncate">
                            ${this.overview}
                        </p>
                        <div
                            class="
                                d-flex
                                justify-content-between
                                align-items-center
                            "
                        >
                            <small class="text-muted">${this.releaseDate}</small>
                        </div>
                    </div>
                </div>
            </div>`;
    }

    printFavoriteCard() {
        return ` <div class="col-12 p-2">
            <div class="card shadow-sm">
                <img src="${this.poster}"/>
                <svg
                        data-movieid="${this.id}"
                        xmlns="http://www.w3.org/2000/svg"
                        stroke="red"
                        fill="red"
                        width="50"
                        height="50"
                        class="bi bi-heart-fill position-absolute p-2"
                        viewBox="0 -2 18 22">
                        <path
                            fill-rule="evenodd"
                            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                        />
                    </svg>
                <div class="card-body">
                    <p class="card-text truncate">
                            ${this.overview}
                        </p>
                    <div
                            class="
                                    d-flex
                                    justify-content-between
                                    align-items-center
                                "
                    >
                        <small class="text-muted">${this.releaseDate}</small>
                    </div>
                </div>
            </div>
        </div>`;
    }
}

interface IMovie {
    poster_path: string;
    id: number;
    overview: string;
    release_date: string;
    title: string;
}
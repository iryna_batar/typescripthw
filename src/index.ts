import { App } from './poster';
import { IStrategy } from './interface_strategy';
import { PopularStrategy } from './popular_strategy';

export async function render(): Promise<void> {
    const strategy : IStrategy = new PopularStrategy();
    const app : App = new App(strategy);
    app.getMovies();
    app.updateFavorite();
}

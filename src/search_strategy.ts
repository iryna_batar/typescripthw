import { IStrategy } from './interface_strategy';
import { Movie } from './movie';
import { RequestHelper } from './requestHelper';

export class SearchStrategy implements IStrategy {
    getMovies(page : number): Promise<Array<Movie>> {
        const input : HTMLElement = document.getElementById("search");
        return new RequestHelper().makeSearchRequest('search/movie', page, input.value);
    }
}
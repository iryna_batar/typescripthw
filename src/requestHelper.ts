import { Movie } from './movie';

export class RequestHelper {
    apiUrl = 'https://api.themoviedb.org/3/';
    apiKey = '?api_key=202407ead7d12c1bf85ac33f98f795a0';

    async makeRequest(requestType: string, page: number): Promise<Array<Movie>> {
        return fetch(this.apiUrl + "movie/" + requestType + this.apiKey + '&page=' + page)
            .then(response => {

                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then(data => {
                const movies: Array<Movie> = [];
                for (const movie of data.results) {
                    movies.push(new Movie(movie));
                }

                return movies;
            });
    }

    async makeSearchRequest(requestType: string, page: number, query : string): Promise<Array<Movie>> {
        return fetch(this.apiUrl + requestType + this.apiKey + '&page=' + page + '&query=' + query)
            .then(response => {

                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then(data => {
                const movies: Array<Movie> = [];
                for (const movie of data.results) {
                    movies.push(new Movie(movie));
                }

                return movies;
            });
    }

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async makeRequestById(id: number) {
        return fetch(this.apiUrl + "movie/" +  id + this.apiKey)
            .then(response => {

                if (!response.ok) {
                    throw new Error(response.statusText);
                }

                return response.json();
            })
            .then(data => {
                return new Movie(data);
            });
    }
}